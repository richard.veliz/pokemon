export interface MenuInterface {
  path: string;
  title: string;
  icon: string;
  rolId?: number;
  class: string;
}
