import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {FooterComponent} from './layouts/footer/footer.component';
import {NavbarComponent} from './layouts/navbar/navbar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminLayoutComponent} from './layouts/adminlayout/adminlayout.component';
import {SpinnerViewComponent} from './gui/spinner/spinnerview.component';
import {SharedDirectivesModule} from '../directives/shared-directives.module';
// import {NgxMaskModule} from 'ngx-mask';


const components = [
  FooterComponent,
  NavbarComponent,
  // SidebarComponent,
  AdminLayoutComponent,
  SpinnerViewComponent,
  // IconComponent,
  // PaginatorComponent,
  // HeadComponent,
  // DeleteComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedDirectivesModule,
    // NgxMaskModule.forRoot(),
  ],
  declarations: [components],
  exports: [
    components,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedDirectivesModule,
    // NgxMaskModule
  ]
})
export class ComponentsModule {
}
