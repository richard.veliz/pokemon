export function toBoolean(value){
    value = (value) ? value : false;
    if (typeof value === 'string') {
      value = value === 'true';
    }
    return value;
}

export function isJsonString(str){
  try {
    JSON.parse(str);
  } catch (e) {
      return false;
  }
  return true;
}