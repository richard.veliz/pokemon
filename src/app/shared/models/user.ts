export class User {
  id: number;
  rolId: number;
  email: string;
  name: string;
  password: string;
  identification: string;

  constructor(obj?) {
    if (obj) {
      this.id = (obj.id) ? obj.id : null;
      this.rolId = (obj.rolId) ? obj.rolId : null;
      this.email = (obj.email) ? obj.email : null;
      this.name = (obj.name) ? obj.name : null;
      this.password = (obj.password) ? obj.password : null;
      this.identification = (obj.identification) ? obj.identification : null;
    }
  }
}
