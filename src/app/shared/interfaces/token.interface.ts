import {User} from "../models/user";

export interface TokenInterface {
  accessToken: string;
  user: User
}
