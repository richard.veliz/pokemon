import {Injectable, NgZone} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})

export class MessageService {

  constructor(public snackBar: MatSnackBar, private zone: NgZone) {
  }

  public MENSAJE_CAMPOS_OBLIGATORIOS = 'Por favor, ingrese los campos obligatorios.';

  openSnackBar(message: string, action?: string, time?: number) {
    this.snackBar.open(message, action, {
        duration: (time) ? time : 5000,
      }
    );
  }

  showSuccess(message: string): void {
    this.zone.run(() => this.snackBar.open(message, 'X', {panelClass: ['mat-bg-success'], duration: 2000}));
  }

  showError(message: string): void {
    this.zone.run(() => this.snackBar.open(message, 'X', {panelClass: ['mat-bg-warn'], duration: 3000}));
  }

  showInfo(message: string, duration = 2000): void {
    this.zone.run(() => this.snackBar.open(message, 'X', {panelClass: ['mat-bg-info'], duration}));
  }

  showPrimary(message: string): void {
    this.zone.run(() => this.snackBar.open(message, 'X', {panelClass: ['mat-bg-primary'], duration: 2000}));
  }
}
