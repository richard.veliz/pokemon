import {NgModule, Type} from '@angular/core';
import {PokemonRoutes} from "./pokemon.routing";
import {RouterModule} from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import {SharedModule} from "../shared/shared.module";
import {DashboardComponent} from "./components/dashboard/dashboard.component";

const components: Array<Type<any>> = [
  DashboardComponent
];

@NgModule({
  imports: [
    SharedModule,
    HttpClientModule,
    RouterModule.forChild(PokemonRoutes),
  ],
  declarations: [components],
  providers: []
})

export class PokemonModule {
}
