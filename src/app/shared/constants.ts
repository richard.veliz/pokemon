export class Constants {
  public static EMAIL_PATTERN = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  public static ALPHANUMERIC_PATTERN = /^[a-zA-Z0-9]*$/;
  public static NUMERIC_PATTERN = /^-?[\d.]+(?:e-?\d+)?$/;
  public static ALPHASPACE_PATTERN = /^[a-zA-Z áéíóúñü]+$/;
  public static PHONE_PATTERN = /[0-9\+\-\ ]/;
  public static ROL_ADMIN = 1;
  public static ROL_EMPLOYEE = 2;
}
